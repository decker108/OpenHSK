OpenHSK
=======
A free and open-source Android app for learning mandarin (the official language of the P. Rep. of China) using the Hanyu Shuiping Kaoshi standardized test for mandarin.


TODO:<br><ul>
<li><strike>DIY word lists using a simple in-app GUI</strike>(DONE)<br>
<li><strike>Add/edit/delete word lists</strike>(DONE)<br>
<li><strike>Add/edit/delete words</strike>(DONE)<br>
<li><strike>Exams</strike>(DONE)<br> 
<li><strike>Better exam results layout</strike>(DONE)<br> 
<li><strike>Exam results statistics (results over time)</strike>(DONE)<br> 
<li><strike>Importing exported word lists</strike>(DONE)
<li>Better UI with action bar
<li>"Reverse" quiz<br> 
<li>Select words in word lists and delete them or move them to existing/new word lists<br>
</ul>

Issues:<ul>
<li>No tabs in exam history view due to lack of app compat v7
</ul>

Dependencies:

This app contains a heavily modified version of AndroidPieChart by Ken Yang (copyright 2013 Ken Yang), used in compliance with the Apache License v2.0.

This app contains a heavily modified version of HoloGraphLibrary by Daniel Nadeau, used in compliance with the Apache License v2.0.
