package edu.openhsk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import edu.openhsk.models.Hanzi;
import edu.openhsk.repository.DatabaseHelper;

public class CharacterViewActivity extends Activity {
	private static final String LOG_TAG = CharacterViewActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.singlechar);
		setTitle(getString(R.string.random_word));
		
		Intent intent = getIntent();
		int id = intent.getIntExtra("edu.openhsk.randomindex", 1);
        
		DatabaseHelper dbh = new DatabaseHelper(this);
		Hanzi charData = dbh.getWordById(id);
		if (charData != null) {
			TextView tv = (TextView) findViewById(R.id.charView);
			tv.setText(charData.getWord());
			
			TextView tv2 = (TextView) findViewById(R.id.pinyinView);
			tv2.setText(charData.getPinyin());
			
			TextView tv1 = (TextView) findViewById(R.id.descrView);
			tv1.setText(charData.getDefinition());
		} else {
			String errorMsg = "No hanzi found for id: " + id;
			Log.e(LOG_TAG, errorMsg);
			Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
		}
	}

}
