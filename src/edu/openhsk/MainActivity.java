package edu.openhsk;

import static edu.openhsk.repository.DatabaseMetadata.ALL_WORD_LIST_IDS;
import static edu.openhsk.repository.DatabaseMetadata.WORD_LIST_ID_CL;
import static edu.openhsk.utils.CSVParser.COMMA_DELIMITER;
import static edu.openhsk.utils.CSVParser.SEMICOLON_DELIMITER;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import edu.openhsk.repository.DatabaseHelper;
import edu.openhsk.utils.CSVParser;

public class MainActivity extends Activity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
	private DatabaseHelper dbh;
	
	private static final Integer[] ALL_DICT_FILE_RES_IDS = { R.string.filename_hsk1,
			R.string.filename_hsk2, R.string.filename_su1,
			R.string.filename_su2, R.string.filename_su2_long,
			R.string.filename_classf };
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        dbh = new DatabaseHelper(this);
        
        Integer[] wordListsToBeUpdated = dbh.doWordListHealthCheck();
		if (wordListsToBeUpdated.length == 0) {
			Log.d(LOG_TAG, "No data needed to be loaded");
			LinearLayout foreground = (LinearLayout) findViewById(R.id.main_menu_layout);
			foreground.setVisibility(View.VISIBLE);
		} else {
			new AsyncParser().execute(wordListsToBeUpdated);
		}
        
        Button listButton = (Button) findViewById(R.id.listButton);
        listButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, WordListSelectionActivity.class);
				intent.putExtra(WordListSelectionActivity.WORD_LIST_SELECTION_MODE_EXTRA, 
						WordListSelectionActivity.LIST_MODE);
				startActivity(intent);
			}
		});
        
        Button charButton = (Button) findViewById(R.id.charButton);
        charButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, CharacterViewActivity.class);
				Random rand = new Random();
				i.putExtra("edu.openhsk.randomindex", rand.nextInt(151)+1);
				startActivity(i);
			}
		});
        
        Button quizButton = (Button) findViewById(R.id.quizButton);
        quizButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, WordListSelectionActivity.class);
				intent.putExtra(WordListSelectionActivity.WORD_LIST_SELECTION_MODE_EXTRA, 
						WordListSelectionActivity.QUIZ_MODE);
				startActivity(intent);
			}
		});
        
        Button examButton = (Button) findViewById(R.id.examButton);
        examButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, WordListSelectionActivity.class);
				intent.putExtra(WordListSelectionActivity.WORD_LIST_SELECTION_MODE_EXTRA, 
						WordListSelectionActivity.EXAM_MODE);
				startActivity(intent);
			}
		});
    }

	public void parseDictionaryFiles(Integer[] indexesToUpdate) {
        try {	
			CSVParser parser = new CSVParser(dbh);
        	BufferedReader br;
			
        	long startTime = System.currentTimeMillis();
			for (int i = 0; i < indexesToUpdate.length; i++) {
				br = new BufferedReader(new InputStreamReader(this.getAssets()
						.open(getString(ALL_DICT_FILE_RES_IDS[indexesToUpdate[i]])), "UTF8"));
				parser.parseWordListCSV(br, ALL_WORD_LIST_IDS[indexesToUpdate[i]], 
						ALL_WORD_LIST_IDS[indexesToUpdate[i]] == WORD_LIST_ID_CL ? SEMICOLON_DELIMITER : COMMA_DELIMITER);
			}
			long endTime = System.currentTimeMillis() - startTime;
			Log.d(LOG_TAG, "Parsing and storing took " + endTime + " ms");
		} catch (IOException e) {
			Toast.makeText(this, R.string.fatal_parser_error, Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	
	private class AsyncParser extends AsyncTask<Integer,Integer,Boolean> {
		private final ProgressDialog dialog;
		
		public AsyncParser() {
			dialog = new ProgressDialog(MainActivity.this);
		}
		
		@Override
		protected void onPreExecute() {
			this.dialog.setMessage(MainActivity.this.getText(R.string.installing_msg));
			this.dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			this.dialog.show();
		}
		
		@Override
		protected Boolean doInBackground(Integer... params) {
			if (params.length != 0) {
				parseDictionaryFiles(params);
			}
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
			
			LinearLayout foreground = (LinearLayout) findViewById(R.id.main_menu_layout);
			foreground.setVisibility(View.VISIBLE);
		}
	}
}